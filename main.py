# -*- coding: utf-8 -*-

from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
import time
from numpy import *
from numpy.linalg import *
import pypot.robot
import keyboard
import sys
from Kinetic import extractPoints,cost_derivative,MGD_l_arm,MGD_r_arm,MGD_l_leg,MGD_r_leg
import pyttsx
import logging

def kcallback(e):
    global pressed_key
    if e.event_type == 'down':
        pressed_key = e.name
    else:
        pressed_key = ''

logging.basicConfig(filename = "poppy"+time.strftime("%Y_%m_%d", time.localtime())+".log",level=logging.DEBUG)
        
k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Body)
logging.info("Kinect lance")
poppy = pypot.robot.from_json('poppy_install.json')
logging.info("Poppy lance")
e = pyttsx.init()
logging.info("Speaker lance")
keyboard.hook(kcallback)
logging.info("clavier lance")
e.say(u'Ah, jai passais une bonne nuit.')
e.runAndWait()

spot = array([-2.05575421  ,0.05247026 ,-1.75366127])
state = 0
temp = 0.0
dt = 0.01
tpose = 2.0
t = time.clock()
speed = 100
K = 200.0
pressed_key = ''

members = {}
members["left arm"] =  ['l_shoulder_y','l_shoulder_x','l_arm_z','l_elbow_y','l_wrist_z','l_wrist_x']
members["right arm"] = ['r_shoulder_y','r_shoulder_x','r_arm_z','r_elbow_y','r_wrist_z','r_wrist_x']
members["left leg"] =  ['l_hip_x','l_hip_z','l_hip_y','l_knee_y','l_ankle_y']
members["right leg"] = ['r_hip_x','r_hip_z','r_hip_y','r_knee_y','r_ankle_y']

q_r_arm_min = array([-80,0  ,-70,-150,-45,-70])
q_r_arm_max = array([80 ,90 ,70 ,-20  ,45 ,70 ])
q_l_arm_min = array([-80,-90,-70,20,-45,-70])
q_l_arm_max = array([80 ,0  ,70 ,150  ,45 ,70 ])
q_r_leg_min = array([-10,-10,-80,0 ,-10])
q_r_leg_max = array([10 ,10 ,0 ,90,10 ])
q_l_leg_min = array([-10,-10,-80,0 ,-10])
q_l_leg_max = array([10 ,10 ,0 ,90,10 ])

posezero={}
posezero['left arm']=array([0,0,0,0,0,0])
posezero['right arm']=array([0,0,0,0,0,0])
posezero['left leg']=array([0,0,0,0,0])
posezero['right leg']=array([0,0,0,0,0])
posequiet={}
posequiet['left arm']=array([89,-80,28,25,-2,-14])
posequiet['right arm']=array([78,84,23,-21,-3,7])
posequiet['left leg']=array([-3,10,-7,12,-1])
posequiet['right leg']=array([2,-9,-13,15,1])

while True:
    try:
        # sensors
        # time
        t = time.clock()
        # kinect
        seeBody = False
        if k.has_new_body_frame():
            bs = k.get_last_body_frame()
            if bs is not None:
                for b in bs.bodies:
                    if not b.is_tracked:
                        continue
                    # get joints positions
                    js = b.joints
                    kpos = extractPoints(js)
                    #print kpos["spine_base"]
                    if norm(kpos["spine_base"]-spot)<0.5:
                        seeBody = True
                        break
                        
        # parameters Poppy
        q_l_arm = array([0.0,0.0,0.0,0.0,0.0,0.0])
        q_r_arm = array([0.0,0.0,0.0,0.0,0.0,0.0])
        q_l_leg = array([0.0,0.0,0.0,0.0,0.0])
        q_r_leg = array([0.0,0.0,0.0,0.0,0.0])
        temp_t = 0.0
        for i in range(len(members["left arm"])):
            temp = max(temp,poppy.__getattribute__(members["left arm"][i]).present_temperature)
            q_l_arm[i] = poppy.__getattribute__(members["left arm"][i]).present_position
        for i in range(len(members["right arm"])):
            temp = max(temp,poppy.__getattribute__(members["right arm"][i]).present_temperature)
            q_r_arm[i] = poppy.__getattribute__(members["right arm"][i]).present_position
        for i in range(len(members["left leg"])):
            max(temp,poppy.__getattribute__(members["left leg"][i]).present_temperature)
            q_l_leg[i] = poppy.__getattribute__(members["left leg"][i]).present_position
        for i in range(len(members["right leg"])):
            temp = max(temp,poppy.__getattribute__(members["right leg"][i]).present_temperature)
            q_r_leg[i] = poppy.__getattribute__(members["right leg"][i]).present_position
        # filter the temperature
        temp = 0.9*temp+0.1*temp_t
            
        #derivative of costs with respect to q
        if seeBody:
            dcdl_l_arm = cost_derivative(q_l_arm,"left arm",kpos)
            dcdl_r_arm = cost_derivative(q_r_arm,"right arm",kpos)
            dcdl_l_leg = cost_derivative(q_l_leg,"left leg",kpos)
            dcdl_r_leg = cost_derivative(q_r_leg,"right leg",kpos)
        else:
            dcdl_l_arm = array([0.0,0.0,0.0,0.0,0.0,0.0])
            dcdl_r_arm = array([0.0,0.0,0.0,0.0,0.0,0.0])
            dcdl_l_leg = array([0.0,0.0,0.0,0.0,0.0])
            dcdl_r_leg = array([0.0,0.0,0.0,0.0,0.0])
            
        # state machine
        if state == 0:
            # sleeping
            q_l_armc = array([NaN,NaN,NaN,NaN,NaN,NaN])
            q_r_armc = array([NaN,NaN,NaN,NaN,NaN,NaN])
            q_l_legc = array([NaN,NaN,NaN,NaN,NaN])
            q_r_legc = array([NaN,NaN,NaN,NaN,NaN])
            if len(pressed_key)>0:
                e.say('Reculez jusco trace de pas sil vous plait.')
                e.runAndWait()
            if seeBody:
                time.sleep(2)
                logging.info("Personne vue")
                if temp < 50:
                    q_l_armc = q_l_arm
                    q_r_armc = q_r_arm
                    q_l_legc = q_l_leg
                    q_r_legc = q_r_leg
                    list_pose = []
                    state = 1
                    logging.info("state : "+str(state))
                else:
                    logging.info("Alerte temperature : "+str(temp))
                    e.say('Mais j''ai trop chaud.')
                    e.runAndWait()
                
        elif state == 1:
            # puppet mode
            if not isnan(dcdl_l_arm.sum()):
                q_l_armc = q_l_armc - K * dcdl_l_arm
            if not isnan(dcdl_r_arm.sum()):
                q_r_armc = q_r_armc - K * dcdl_r_arm
            if not isnan(dcdl_l_leg.sum()):
                q_l_legc = q_l_legc - K * dcdl_l_leg
            q_l_legc[4]=NaN
            if not isnan(dcdl_r_leg.sum()): 
                q_r_legc = q_r_legc - K * dcdl_r_leg
            q_r_legc[4]=NaN
            if not seeBody:
                pose0=[]
                posecur={}
                posecur['left arm']=q_l_arm
                posecur['right arm']=q_r_arm
                posecur['left leg']=q_l_leg
                posecur['right leg']=q_r_leg
                pose0.append(posecur)
                pose0.append(posezero)
                pose0.append(posequiet)
                t0 = t
                state = 4
                logging.info("state : "+str(state))
            if temp > 60:
                logging.info("Alerte temperature : "+str(temp))
                e.say('Oh non, Je m arrete, j ai vraiment trop chaud.')
                e.runAndWait()
                t0 = t
                state = 0
                logging.info("state : "+str(state))
        elif state == 2:
            # mou
            q_l_armc = q_l_arm
            q_r_armc = q_r_arm
            q_l_legc = q_l_leg
            q_r_legc = q_r_leg
            if t>t0+5:
                state = 0
                logging.info("state : "+str(state))
                
        elif state == 3:
            trel = (t-t0)/tpose
            i=int(trel)
            w=trel-i
            if i>len(list_pose)-2:
                list_pose = []
                state = 0
                logging.info("state : "+str(state))
            else:
                q_l_armc = (1-w)*list_pose[i]["left arm"]+(w)*list_pose[i+1]["left arm"]
                q_r_armc = (1-w)*list_pose[i]["right arm"]+(w)*list_pose[i+1]["right arm"]
                q_l_legc = (1-w)*list_pose[i]["left leg"]+(w)*list_pose[i+1]["left leg"]
                q_r_legc = (1-w)*list_pose[i]["right leg"]+(w)*list_pose[i+1]["right leg"]
            if temp > 60:
                e.say('Oh non, Je m''arrête, j''ai vraiment trop chaud.')
                e.runAndWait()
                t0 = t
                speed = 50
                state = 0
                logging.info("state : "+str(state))
        elif state == 4:
            trel = (t-t0)/tpose
            i=int(trel)
            w=trel-i
            if i>len(pose0)-2:
                pose0 = []
                state = 0
                logging.info("state : "+str(state))
            else:
                q_l_armc = (1-w)*pose0[i]["left arm"]+(w)*pose0[i+1]["left arm"]
                q_r_armc = (1-w)*pose0[i]["right arm"]+(w)*pose0[i+1]["right arm"]
                q_l_legc = (1-w)*pose0[i]["left leg"]+(w)*pose0[i+1]["left leg"]
                q_r_legc = (1-w)*pose0[i]["right leg"]+(w)*pose0[i+1]["right leg"]
            if temp > 60:
                logging.info("Alerte temperature : "+str(temp))
                e.say('Oh non, Je m''arrête, j''ai vraiment trop chaud.')
                e.runAndWait()
                t0 = t
                speed = 50
                state = 0
                logging.info("state : "+str(state))
        
        # actuation
        for i in range(len(members["left arm"])):
            mot = poppy.__getattribute__(members["left arm"][i])
            if isnan(q_l_armc[i]):
                mot.compliant = True
            else:
                mot.compliant = False
                if q_l_armc[i]>q_l_arm_max[i]:
                    q_l_armc[i]=q_l_arm_max[i]
                if q_l_armc[i]<q_l_arm_min[i]:
                    q_l_armc[i]=q_l_arm_min[i]
                mot.goal_position = q_l_armc[i]
                mot.moving_speed = speed
        for i in range(len(members["right arm"])):
            mot = poppy.__getattribute__(members["right arm"][i])
            if isnan(q_r_armc[i]):
                mot.compliant = True
            else:
                mot.compliant = False
                if q_r_armc[i]>q_r_arm_max[i]:
                    q_r_armc[i]=q_r_arm_max[i]
                if q_r_armc[i]<q_r_arm_min[i]:
                    q_r_armc[i]=q_r_arm_min[i]
                mot.goal_position = q_r_armc[i]
                mot.moving_speed = speed
        for i in range(len(members["left leg"])):
            mot = poppy.__getattribute__(members["left leg"][i])
            if isnan(q_l_legc[i]):
                mot.compliant = True
            else:
                mot.compliant = False
                if q_l_legc[i]>q_l_leg_max[i]:
                    q_l_legc[i]=q_l_leg_max[i]
                if q_l_legc[i]<q_l_leg_min[i]:
                    q_l_legc[i]=q_l_leg_min[i]
                mot.goal_position = q_l_legc[i]  
                mot.moving_speed = speed
        for i in range(len(members["right leg"])):
            mot = poppy.__getattribute__(members["right leg"][i])
            if isnan(q_r_legc[i]):
                mot.compliant = True
            else:
                mot.compliant = False
                if q_r_legc[i]>q_r_leg_max[i]:
                    q_r_legc[i]=q_r_leg_max[i]
                if q_r_legc[i]<q_r_leg_min[i]:
                    q_r_legc[i]=q_r_leg_min[i]
                mot.goal_position = q_r_legc[i]
                mot.moving_speed = speed
        print "Temperature : "+str(temp)+"degC"
        time.sleep(dt)
    except Exception as e:
        logging.info(e)
        logging.info("deriv left arm"+str(dcdl_l_arm))
        logging.info("deriv right arm"+str(dcdl_r_arm))
        logging.info("deriv left leg"+str(dcdl_l_leg))
        logging.info("deriv right leg"+str(dcdl_r_leg))