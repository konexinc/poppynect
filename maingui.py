# -*- coding: utf-8 -*-

from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
import time,sys,ctypes
from numpy import *
from numpy.linalg import *
import pypot.robot
from Kinetic import extractPoints,cost_derivative,MGD_l_arm,MGD_r_arm,MGD_l_leg,MGD_r_leg
import pyttsx
import logging
import pygame

def draw_body_bone(frame, joints, jointPoints, color, joint0, joint1):
    joint0State = joints[joint0].TrackingState;
    joint1State = joints[joint1].TrackingState;

    # both joints are not tracked
    if (joint0State == PyKinectV2.TrackingState_NotTracked) or (joint1State == PyKinectV2.TrackingState_NotTracked): 
        return

    # both joints are not *really* tracked
    if (joint0State == PyKinectV2.TrackingState_Inferred) and (joint1State == PyKinectV2.TrackingState_Inferred):
        return

    # ok, at least one is good 
    start = (jointPoints[joint0].x, jointPoints[joint0].y)
    end = (jointPoints[joint1].x, jointPoints[joint1].y)

    try:
        pygame.draw.line(frame, color, start, end, 8)
    except: # need to catch it due to possible invalid positions (with inf)
        pass

def draw_body(frame, joints, jointPoints, color):
    # Torso
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_Head, PyKinectV2.JointType_Neck);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_Neck, PyKinectV2.JointType_SpineShoulder);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder, PyKinectV2.JointType_SpineMid);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_SpineMid, PyKinectV2.JointType_SpineBase);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder, PyKinectV2.JointType_ShoulderRight);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder, PyKinectV2.JointType_ShoulderLeft);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_SpineBase, PyKinectV2.JointType_HipRight);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_SpineBase, PyKinectV2.JointType_HipLeft);

    # Right Arm    
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_ShoulderRight, PyKinectV2.JointType_ElbowRight);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_ElbowRight, PyKinectV2.JointType_WristRight);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_WristRight, PyKinectV2.JointType_HandRight);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_HandRight, PyKinectV2.JointType_HandTipRight);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_WristRight, PyKinectV2.JointType_ThumbRight);

    # Left Arm
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_ShoulderLeft, PyKinectV2.JointType_ElbowLeft);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_ElbowLeft, PyKinectV2.JointType_WristLeft);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_WristLeft, PyKinectV2.JointType_HandLeft);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_HandLeft, PyKinectV2.JointType_HandTipLeft);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_WristLeft, PyKinectV2.JointType_ThumbLeft);

    # Right Leg
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_HipRight, PyKinectV2.JointType_KneeRight);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_KneeRight, PyKinectV2.JointType_AnkleRight);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_AnkleRight, PyKinectV2.JointType_FootRight);

    # Left Leg
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_HipLeft, PyKinectV2.JointType_KneeLeft);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_KneeLeft, PyKinectV2.JointType_AnkleLeft);
    draw_body_bone(frame,joints, jointPoints, color, PyKinectV2.JointType_AnkleLeft, PyKinectV2.JointType_FootLeft);

# init
spot = array([-2.05575421  ,0.05247026 ,-1.75366127])
state = 0
temp = 0.0
dt = 0.01
tpose = 2.0
t = time.clock()
speed = 100
K = 200.0
go_on = True
kin = None
rob = None
bodies = None
logText = []

# description of the robot
members = {}
members["left arm"] =  ['l_shoulder_y','l_shoulder_x','l_arm_z','l_elbow_y','l_wrist_z','l_wrist_x']
members["right arm"] = ['r_shoulder_y','r_shoulder_x','r_arm_z','r_elbow_y','r_wrist_z','r_wrist_x']
members["left leg"] =  ['l_hip_x','l_hip_z','l_hip_y','l_knee_y','l_ankle_y']
members["right leg"] = ['r_hip_x','r_hip_z','r_hip_y','r_knee_y','r_ankle_y']

description = { 41:u"épaule gauche1",42:u"épaule gauche2",43:u"bras gauche",44:u"coude gauche",1:u"poignet gauche1",2:u"poignet gauche2",
                51:u"épaule droite1",52:u"épaule droite2",53:u"bras droit",54:u"coude droit",3:u"poignet droit",4:u"poignet droit",
                11:u"cuisse gauche1",12:u"cuisse gauche2",13:u"cuisse gauche3",14:u"genou gauche",15:u"cheville gauche",
                21:u"cuisse droite1",22:u"cuisse droite2",23:u"cuisse droite3",24:u"genou droit",25:u"cheville droite"}

q_r_arm_min = array([-80,0  ,-70,-150,-45,-70])
q_r_arm_max = array([80 ,90 ,70 ,-20  ,45 ,70 ])
q_l_arm_min = array([-80,-90,-70,20,-45,-70])
q_l_arm_max = array([80 ,0  ,70 ,150  ,45 ,70 ])
q_r_leg_min = array([-10,-10,-80,0 ,-10])
q_r_leg_max = array([10 ,10 ,0 ,90,10 ])
q_l_leg_min = array([-10,-10,-80,0 ,-10])
q_l_leg_max = array([10 ,10 ,0 ,90,10 ])

posezero={}
posezero['left arm']=array([0,0,0,0,0,0])
posezero['right arm']=array([0,0,0,0,0,0])
posezero['left leg']=array([0,0,0,0,0])
posezero['right leg']=array([0,0,0,0,0])
posequiet={}
posequiet['left arm']=array([89,-80,28,25,-2,-14])
posequiet['right arm']=array([78,84,23,-21,-3,7])
posequiet['left leg']=array([-3,10,-7,12,-1])
posequiet['right leg']=array([2,-9,-13,15,1])
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
BLUE =  (103,185,205)
GREEN = (  0, 255,   0)
RED =   (255,   0,   0)
YELLOW = (255, 255, 0)
#logging
logging.basicConfig(filename = "poppy"+time.strftime("%Y_%m_%d", time.localtime())+".log",level=logging.DEBUG)
#pygame
pygame.init()
infoObject = pygame.display.Info()
screen = pygame.display.set_mode((0,0),pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.FULLSCREEN,32)
pygame.display.set_caption("Poppynect")
largeText = pygame.font.Font('GIL.TTF',50)

#speaker
e = pyttsx.init()
#kinect
kin = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Color | PyKinectV2.FrameSourceTypes_Body)
frame_surface = pygame.Surface((kin.color_frame_desc.Width, kin.color_frame_desc.Height), 0, 32)
while go_on:
    time.sleep(dt)
    t = time.clock()
    logText = []
    logText.append(u"Etat: "+str(state))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            go_on = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                go_on = False
    seeBody = False
    if kin.has_new_body_frame():
        bodies = kin.get_last_body_frame()
        if bodies is not None:
            for b in bodies.bodies:
                if not b.is_tracked:
                    continue
                # get joints positions
                js = b.joints
                kpos = extractPoints(js)
                #print kpos["spine_base"]
                if norm(kpos["spine_base"]-spot)<0.5:
                    seeBody = True
                    break
                    
    
    # finite state machine
    if state == 0:
        # get the robot
        try:
            rob = pypot.robot.from_json('poppy_install.json')
        except IndexError as error:
            i_lost_motor=map(int,error.message[error.message.find('missing')+9:-3].split(","))
            logText.append(u"Moteurs perdus :")
            for i in i_lost_motor:
                logText.append(description[i])
            rob = None
            pass
        if not(rob is None):
            state = 1
            q_l_armc = array([NaN,NaN,NaN,NaN,NaN,NaN])
            q_r_armc = array([NaN,NaN,NaN,NaN,NaN,NaN])
            q_l_legc = array([NaN,NaN,NaN,NaN,NaN])
            q_r_legc = array([NaN,NaN,NaN,NaN,NaN])
    elif state == 1:
        # poppy mou
        q_l_arm = array([0.0,0.0,0.0,0.0,0.0,0.0])
        q_r_arm = array([0.0,0.0,0.0,0.0,0.0,0.0])
        q_l_leg = array([0.0,0.0,0.0,0.0,0.0])
        q_r_leg = array([0.0,0.0,0.0,0.0,0.0])
        temp_t = 0.0
        for i in range(len(members["left arm"])):
            temp_t = max(temp_t,rob.__getattribute__(members["left arm"][i]).present_temperature)
            q_l_arm[i] = rob.__getattribute__(members["left arm"][i]).present_position
        for i in range(len(members["right arm"])):
            temp_t = max(temp_t,rob.__getattribute__(members["right arm"][i]).present_temperature)
            q_r_arm[i] = rob.__getattribute__(members["right arm"][i]).present_position
        for i in range(len(members["left leg"])):
            temp_t = max(temp_t,rob.__getattribute__(members["left leg"][i]).present_temperature)
            q_l_leg[i] = rob.__getattribute__(members["left leg"][i]).present_position
        for i in range(len(members["right leg"])):
            temp_t = max(temp_t,rob.__getattribute__(members["right leg"][i]).present_temperature)
            q_r_leg[i] = rob.__getattribute__(members["right leg"][i]).present_position
        # filter the temperature
        temp = 0.9*temp+0.1*temp_t
        logText.append(u"Température du robot : "+str(int(temp))+u"°C")
        # sleeping
        q_l_armc = array([NaN,NaN,NaN,NaN,NaN,NaN])
        q_r_armc = array([NaN,NaN,NaN,NaN,NaN,NaN])
        q_l_legc = array([NaN,NaN,NaN,NaN,NaN])
        q_r_legc = array([NaN,NaN,NaN,NaN,NaN])
        if seeBody and temp < 50:
            q_l_armc = q_l_arm
            q_r_armc = q_r_arm
            q_l_legc = q_l_leg
            q_r_legc = q_r_leg
            list_pose = []
            state = 2
    elif state == 2:
        # poppy miroir
        q_l_arm = array([0.0,0.0,0.0,0.0,0.0,0.0])
        q_r_arm = array([0.0,0.0,0.0,0.0,0.0,0.0])
        q_l_leg = array([0.0,0.0,0.0,0.0,0.0])
        q_r_leg = array([0.0,0.0,0.0,0.0,0.0])
        temp_t = 0.0
        for i in range(len(members["left arm"])):
            temp_t = max(temp_t,rob.__getattribute__(members["left arm"][i]).present_temperature)
            q_l_arm[i] = rob.__getattribute__(members["left arm"][i]).present_position
        for i in range(len(members["right arm"])):
            temp_t = max(temp_t,rob.__getattribute__(members["right arm"][i]).present_temperature)
            q_r_arm[i] = rob.__getattribute__(members["right arm"][i]).present_position
        for i in range(len(members["left leg"])):
            temp_t = max(temp_t,rob.__getattribute__(members["left leg"][i]).present_temperature)
            q_l_leg[i] = rob.__getattribute__(members["left leg"][i]).present_position
        for i in range(len(members["right leg"])):
            temp_t = max(temp_t,rob.__getattribute__(members["right leg"][i]).present_temperature)
            q_r_leg[i] = rob.__getattribute__(members["right leg"][i]).present_position
        # filter the temperature
        temp = 0.9*temp+0.1*temp_t
        logText.append(u"Température du robot : "+str(int(temp))+u"°C")
        # jacobian
        dcdl_l_arm = cost_derivative(q_l_arm,"left arm",kpos)
        dcdl_r_arm = cost_derivative(q_r_arm,"right arm",kpos)
        dcdl_l_leg = cost_derivative(q_l_leg,"left leg",kpos)
        dcdl_r_leg = cost_derivative(q_r_leg,"right leg",kpos)
        if not isnan(dcdl_l_arm.sum()):
            q_l_armc = q_l_armc - K * dcdl_l_arm
        if not isnan(dcdl_r_arm.sum()):
            q_r_armc = q_r_armc - K * dcdl_r_arm
        if not isnan(dcdl_l_leg.sum()):
            q_l_legc = q_l_legc - K * dcdl_l_leg
        q_l_legc[4]=NaN
        if not isnan(dcdl_r_leg.sum()): 
            q_r_legc = q_r_legc - K * dcdl_r_leg
        q_r_legc[4]=NaN
        if (not seeBody) or (temp > 60):
            pose0=[]
            posecur={}
            posecur['left arm']=q_l_arm
            posecur['right arm']=q_r_arm
            posecur['left leg']=q_l_leg
            posecur['right leg']=q_r_leg
            pose0.append(posecur)
            pose0.append(posezero)
            pose0.append(posequiet)
            t0 = t
            state = 3
    elif state == 3:
        #denouage
        trel = (t-t0)/tpose
        i=int(trel)
        w=trel-i
        if i>len(pose0)-2:
            pose0 = []
            state = 1
            logging.info("state : "+str(state))
        else:
            q_l_armc = (1-w)*pose0[i]["left arm"]+(w)*pose0[i+1]["left arm"]
            q_r_armc = (1-w)*pose0[i]["right arm"]+(w)*pose0[i+1]["right arm"]
            q_l_legc = (1-w)*pose0[i]["left leg"]+(w)*pose0[i+1]["left leg"]
            q_r_legc = (1-w)*pose0[i]["right leg"]+(w)*pose0[i+1]["right leg"]
    
    
    # actuation
    if state > 0:
        for i in range(len(members["left arm"])):
            mot = rob.__getattribute__(members["left arm"][i])
            if isnan(q_l_armc[i]):
                mot.compliant = True
            else:
                mot.compliant = False
                if q_l_armc[i]>q_l_arm_max[i]:
                    q_l_armc[i]=q_l_arm_max[i]
                if q_l_armc[i]<q_l_arm_min[i]:
                    q_l_armc[i]=q_l_arm_min[i]
                mot.goal_position = q_l_armc[i]
                mot.moving_speed = speed
        for i in range(len(members["right arm"])):
            mot = rob.__getattribute__(members["right arm"][i])
            if isnan(q_r_armc[i]):
                mot.compliant = True
            else:
                mot.compliant = False
                if q_r_armc[i]>q_r_arm_max[i]:
                    q_r_armc[i]=q_r_arm_max[i]
                if q_r_armc[i]<q_r_arm_min[i]:
                    q_r_armc[i]=q_r_arm_min[i]
                mot.goal_position = q_r_armc[i]
                mot.moving_speed = speed
        for i in range(len(members["left leg"])):
            mot = rob.__getattribute__(members["left leg"][i])
            if isnan(q_l_legc[i]):
                mot.compliant = True
            else:
                mot.compliant = False
                if q_l_legc[i]>q_l_leg_max[i]:
                    q_l_legc[i]=q_l_leg_max[i]
                if q_l_legc[i]<q_l_leg_min[i]:
                    q_l_legc[i]=q_l_leg_min[i]
                mot.goal_position = q_l_legc[i]  
                mot.moving_speed = speed
        for i in range(len(members["right leg"])):
            mot = rob.__getattribute__(members["right leg"][i])
            if isnan(q_r_legc[i]):
                mot.compliant = True
            else:
                mot.compliant = False
                if q_r_legc[i]>q_r_leg_max[i]:
                    q_r_legc[i]=q_r_leg_max[i]
                if q_r_legc[i]<q_r_leg_min[i]:
                    q_r_legc[i]=q_r_leg_min[i]
                mot.goal_position = q_r_legc[i]
                mot.moving_speed = speed    
    
    
    # display the kinect picture
    if kin.has_new_color_frame():
        frame = kin.get_last_color_frame()
        frame_surface.lock()
        address = kin.surface_as_array(frame_surface.get_buffer())
        ctypes.memmove(address, frame.ctypes.data, frame.size)
        del address
        frame_surface.unlock()
    if not bodies is None:
        for body in bodies.bodies:
            if body.is_tracked:
                joints = body.joints
                joint_points = kin.body_joints_to_color_space(joints)
                draw_body(frame_surface,joints, joint_points, YELLOW)
    surface_to_draw = pygame.transform.scale(frame_surface, (screen.get_width(), screen.get_height()));
    screen.blit(surface_to_draw, (0,0))
    surface_to_draw = None
    logo = pygame.image.load('symbole.png')
    logore = pygame.transform.scale(logo,(100,100));
    screen.blit(logore,(screen.get_width()-100, screen.get_height()-100,100,100))
    top = 50
    for line in logText:
        log = largeText.render(line, True, BLUE)
        logRect = log.get_rect()
        logRect.top = top
        logRect.left = 50
        top = logRect.bottom
        screen.blit(log,logRect)
    pygame.display.update()

kin.close()
pygame.quit()
sys.exit()

        