from numpy import *
from numpy.linalg import *
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime

def cross_prod(a,b):
    ax = array([[0,-a[2],a[1]],[a[2],0,-a[0]],[-a[1],a[0],0]])
    return dot(ax,b)
    
def normalized_cross_prod(a,b):
    return cross_prod(a,b)/(norm(cross_prod(a,b)))
    
def normalized_dot(a,b):
    return dot(a,b)/(norm(a)*norm(b))

def toVect(joint):
    angle_cam = -30*pi/180.0
    r=rot_mat('y',sin(angle_cam),cos(angle_cam))
    if joint.TrackingState == PyKinectV2.TrackingState_Tracked:
        return dot(r,array([-joint.Position.z,joint.Position.x,joint.Position.y]))
    else:
        return array([0.0,0.0,0.0])

def rot_mat(axe,s,c):
    if axe == "x":
        return array([[1,0,0],[0,c,-s],[0,s,c]])
    elif axe == "y":
        return array([[c,0,s],[0,1,0],[-s,0,c]])
    elif axe == "z":
        return array([[c,-s,0],[s,c,0],[0,0,1]])
    else:
        return 0
        
def extractPoints(js):
    
    pos = {}
    # mirrored
    pos['l_shoulder'] = toVect(js[PyKinectV2.JointType_ShoulderRight])
    pos['l_elbow'] = toVect(js[PyKinectV2.JointType_ElbowRight])
    pos['l_wrist'] = toVect(js[PyKinectV2.JointType_WristRight])
    pos['l_tip'] = toVect(js[PyKinectV2.JointType_HandTipRight])
    
    pos['l_hip'] = toVect(js[PyKinectV2.JointType_HipRight])
    pos['l_knee'] = toVect(js[PyKinectV2.JointType_KneeRight])
    pos['l_ankle'] = toVect(js[PyKinectV2.JointType_AnkleRight])
    pos['l_toe'] = toVect(js[PyKinectV2.JointType_FootRight])
    
    pos['r_shoulder'] = toVect(js[PyKinectV2.JointType_ShoulderLeft])
    pos['r_elbow'] = toVect(js[PyKinectV2.JointType_ElbowLeft])
    pos['r_wrist'] = toVect(js[PyKinectV2.JointType_WristLeft])
    pos['r_tip'] = toVect(js[PyKinectV2.JointType_HandTipLeft])

    pos['r_hip'] = toVect(js[PyKinectV2.JointType_HipLeft])
    pos['r_knee'] = toVect(js[PyKinectV2.JointType_KneeLeft])
    pos['r_ankle'] = toVect(js[PyKinectV2.JointType_AnkleLeft])
    pos['r_toe'] = toVect(js[PyKinectV2.JointType_FootLeft])
    
    pos['spine_base'] = toVect(js[PyKinectV2.JointType_SpineBase])
    
    return pos

# Poppy direct kinematics
def MGD_l_arm(q_l_arm):
    # shoulder position on y
    ysh = 0.106
    # length of upper arm
    uarm = 0.150
    # length of forearm
    farm = 0.120
    # length of hand
    hand = 0.100
    
    pos = {}
    
    pos['l_shoulder'] = array([0,ysh,0])
    pos['l_elbow'] = array([0,0,0])
    pos['l_wrist'] = array([0,0,0])
    pos['l_tip'] = array([0,0,0])

    #definition of axes
    x=array([1,0,0])
    y=array([0,1,0])
    z=array([0,0,1])
    
    # computation of sin and cos
    s_l_arm = sin(q_l_arm*pi/180.0)
    c_l_arm = cos(q_l_arm*pi/180.0)

    # transforms
    
    # left arm chain
    rot_l_arm_0 = rot_mat("y",s_l_arm[0],c_l_arm[0])
    rot_l_arm_1 = rot_mat("z",s_l_arm[1],c_l_arm[1])
    rot_l_arm_2 = rot_mat("y",s_l_arm[2],c_l_arm[2])
    rot_l_arm_3 = rot_mat("x",s_l_arm[3],c_l_arm[3])
    rot_l_arm_4 = rot_mat("y",s_l_arm[4],c_l_arm[4])
    rot_l_arm_5 = rot_mat("z",s_l_arm[5],c_l_arm[5])
    t_l_arm_0 = rot_l_arm_0
    t_l_arm_1 = dot(t_l_arm_0,rot_l_arm_1)
    t_l_arm_2 = dot(t_l_arm_1,rot_l_arm_2)
    t_l_arm_3 = dot(t_l_arm_2,rot_l_arm_3)
    t_l_arm_4 = dot(t_l_arm_3,rot_l_arm_4)
    t_l_arm_5 = dot(t_l_arm_4,rot_l_arm_5)
    
    # left arm positions
    pos['l_elbow'] = pos['l_shoulder']+uarm*dot(t_l_arm_2,y)
    pos['l_wrist'] = pos['l_elbow']+farm*dot(t_l_arm_3,y)
    pos['l_tip'] = pos['l_wrist']+hand*dot(t_l_arm_5,y)
    
    return pos

def MGD_r_arm(q_r_arm):
    # shoulder position on y
    ysh = 0.106
    # length of upper arm
    uarm = 0.150
    # length of forearm
    farm = 0.120
    # length of hand
    hand = 0.100
    
    pos = {}
    
    pos['r_shoulder'] = array([0,-ysh,0])
    pos['r_elbow'] = array([0,0,0])
    pos['r_wrist'] = array([0,0,0])
    pos['r_tip'] = array([0,0,0])

    #definition of axes
    x=array([1,0,0])
    y=array([0,1,0])
    z=array([0,0,1])
    
    # computation of sin and cos
    s_r_arm = sin(q_r_arm*pi/180.0)
    c_r_arm = cos(q_r_arm*pi/180.0)

    # transforms
    
    # left arm chain
    rot_r_arm_0 = rot_mat("y",s_r_arm[0],c_r_arm[0])
    rot_r_arm_1 = rot_mat("z",s_r_arm[1],c_r_arm[1])
    rot_r_arm_2 = rot_mat("y",s_r_arm[2],c_r_arm[2])
    rot_r_arm_3 = rot_mat("x",s_r_arm[3],c_r_arm[3])
    rot_r_arm_4 = rot_mat("y",s_r_arm[4],c_r_arm[4])
    rot_r_arm_5 = rot_mat("z",s_r_arm[5],c_r_arm[5])
    t_r_arm_0 = rot_r_arm_0
    t_r_arm_1 = dot(t_r_arm_0,rot_r_arm_1)
    t_r_arm_2 = dot(t_r_arm_1,rot_r_arm_2)
    t_r_arm_3 = dot(t_r_arm_2,rot_r_arm_3)
    t_r_arm_4 = dot(t_r_arm_3,rot_r_arm_4)
    t_r_arm_5 = dot(t_r_arm_4,rot_r_arm_5)
    
    # right arm positions
    pos['r_elbow'] = pos['r_shoulder']-uarm*dot(t_r_arm_2,y)
    pos['r_wrist'] = pos['r_elbow']-farm*dot(t_r_arm_3,y)
    pos['r_tip'] = pos['r_wrist']-hand*dot(t_r_arm_5,y)
    
    return pos

def MGD_l_leg(q_l_leg):
    # hip position on z
    zhip = -0.5
    # hip position on y
    yhip = 0.045
    # length of thigh
    zthigh = 0.200
    # shift of thigh
    ythigh = 0.030
    # length of leg
    leg = 0.210
    # toe position on x
    xtoe = 0.120
    
    pos = {}
    pos['spine'] = array([0,0,-zhip])
    pos['l_hip'] = array([0,0,0])
    pos['l_knee'] = array([0,0,0])
    pos['l_ankle'] = array([0,0,0])
    pos['l_toe'] = array([0,0,0])
    
    pos['spine_base'] = array([0,0,zhip])
    
    #definition of axes
    x=array([1,0,0])
    y=array([0,1,0])
    z=array([0,0,1])
    
    # computation of sin and cos
    s_l_leg = sin(q_l_leg*pi/180.0)
    c_l_leg = cos(q_l_leg*pi/180.0)
    
    # transforms
    # left leg chain
    rot_l_leg_0 = rot_mat("x",s_l_leg[0],c_l_leg[0])
    rot_l_leg_1 = rot_mat("z",s_l_leg[1],c_l_leg[1])
    rot_l_leg_2 = rot_mat("y",s_l_leg[2],c_l_leg[2])
    rot_l_leg_3 = rot_mat("y",s_l_leg[3],c_l_leg[3])
    rot_l_leg_4 = rot_mat("y",s_l_leg[4],c_l_leg[4])
    t_l_leg_0 = rot_l_leg_0
    t_l_leg_1 = dot(t_l_leg_0,rot_l_leg_1)
    t_l_leg_2 = dot(t_l_leg_1,rot_l_leg_2)
    t_l_leg_3 = dot(t_l_leg_2,rot_l_leg_3)
    t_l_leg_4 = dot(t_l_leg_3,rot_l_leg_4)

    # left leg positions
    pos['l_hip'] = pos['spine']+yhip*dot(t_l_leg_0,y)
    pos['l_knee'] = pos['l_hip']-zthigh*dot(t_l_leg_2,z)
    pos['l_ankle'] = pos['l_knee']-leg*dot(t_l_leg_3,z)
    pos['l_toe'] = pos['l_ankle']+xtoe*dot(t_l_leg_4,x)
    
    return pos
    
def MGD_r_leg(q_r_leg):
    # hip position on z
    zhip = -0.5
    # hip position on y
    yhip = 0.045
    # length of thigh
    zthigh = 0.200
    # shift of thigh
    ythigh = 0.030
    # length of leg
    leg = 0.210
    # toe position on x
    xtoe = 0.120
    
    pos = {}
    pos['spine'] = array([0,0,-zhip])
    pos['r_hip'] = array([0,0,0])
    pos['r_knee'] = array([0,0,0])
    pos['r_ankle'] = array([0,0,0])
    pos['r_toe'] = array([0,0,0])
    
    pos['spine_base'] = array([0,0,zhip])
    
    #definition of axes
    x=array([1,0,0])
    y=array([0,1,0])
    z=array([0,0,1])
    
    # computation of sin and cos
    s_r_leg = sin(q_r_leg*pi/180.0)
    c_r_leg = cos(q_r_leg*pi/180.0)
    
    # transforms
    # right leg chain
    rot_r_leg_0 = rot_mat("x",s_r_leg[0],c_r_leg[0])
    rot_r_leg_1 = rot_mat("z",s_r_leg[1],c_r_leg[1])
    rot_r_leg_2 = rot_mat("y",s_r_leg[2],c_r_leg[2])
    rot_r_leg_3 = rot_mat("y",s_r_leg[3],c_r_leg[3])
    rot_r_leg_4 = rot_mat("y",s_r_leg[4],c_r_leg[4])
    t_r_leg_0 = rot_r_leg_0
    t_r_leg_1 = dot(t_r_leg_0,rot_r_leg_1)
    t_r_leg_2 = dot(t_r_leg_1,rot_r_leg_2)
    t_r_leg_3 = dot(t_r_leg_2,rot_r_leg_3)
    t_r_leg_4 = dot(t_r_leg_3,rot_r_leg_4)
    
    # right leg positions
    pos['r_hip'] = pos['spine']-yhip*dot(t_r_leg_0,y)
    pos['r_knee'] = pos['r_hip']-zthigh*dot(t_r_leg_2,z)
    pos['r_ankle'] = pos['r_knee']-leg*dot(t_r_leg_3,z)
    pos['r_toe'] = pos['r_ankle']+xtoe*dot(t_r_leg_4,x)
    
    return pos

def cost_l_arm(q_l_arm,kpos):
    ymilieu = 0.1
    yleft = 0.35
    xback = 0.10
    xfront = 0.50
    ppos = MGD_l_arm(q_l_arm)
    kuarm = kpos["l_elbow"]-kpos["l_shoulder"]
    puarm = ppos["l_elbow"]-ppos["l_shoulder"]
    kfarm = kpos["l_wrist"]-kpos["l_elbow"]
    pfarm = ppos["l_wrist"]-ppos["l_elbow"]
    khand = kpos["l_tip"]-kpos["l_wrist"]
    phand = ppos["l_tip"]-ppos["l_wrist"]
    
    constraint = 0
    if ppos["l_tip"][1]<ymilieu:
        constraint = constraint + (ppos["l_tip"][1]-ymilieu)**2
    if ppos["l_tip"][1]>yleft:
        constraint = constraint + (ppos["l_tip"][1]-yleft)**2
    if ppos["l_tip"][0]<xback:
        constraint = constraint + (ppos["l_tip"][0]-xback)**2
    if ppos["l_tip"][0]>xfront:
        constraint = constraint + (ppos["l_tip"][0]-xfront)**2
        
    if ppos["l_wrist"][1]<ymilieu:
        constraint = constraint + (ppos["l_wrist"][1]-ymilieu)**2
    if ppos["l_wrist"][1]>yleft:
        constraint = constraint + (ppos["l_wrist"][1]-yleft)**2
    if ppos["l_wrist"][0]<xback:
        constraint = constraint + (ppos["l_wrist"][0]-xback)**2
    if ppos["l_wrist"][0]>xfront:
        constraint = constraint + (ppos["l_wrist"][0]-xfront)**2
    return {'constraint':constraint, 'upper_arm':-normalized_dot(kuarm,puarm),'fore_arm':-normalized_dot(kfarm,pfarm),'hand':-normalized_dot(khand,phand)}

def cost_r_arm(q_r_arm,kpos):
    ymilieu = -0.1
    yright = -0.35
    xback = 0.10
    xfront = 0.50
    ppos = MGD_r_arm(q_r_arm)
    kuarm = kpos["r_elbow"]-kpos["r_shoulder"]
    puarm = ppos["r_elbow"]-ppos["r_shoulder"]
    kfarm = kpos["r_wrist"]-kpos["r_elbow"]
    pfarm = ppos["r_wrist"]-ppos["r_elbow"]
    khand = kpos["r_tip"]-kpos["r_wrist"]
    phand = ppos["r_tip"]-ppos["r_wrist"]
    
    constraint = 0
    if ppos["r_tip"][1]>ymilieu:
        constraint = constraint + (ppos["r_tip"][1]-ymilieu)**2
    if ppos["r_tip"][1]<yright:
        constraint = constraint + (ppos["r_tip"][1]-yright)**2
    if ppos["r_tip"][0]<xback:
        constraint = constraint + (ppos["r_tip"][0]-xback)**2
    if ppos["r_tip"][0]>xfront:
        constraint = constraint + (ppos["r_tip"][0]-xfront)**2
        
    if ppos["r_wrist"][1]>ymilieu:
        constraint = constraint + (ppos["r_wrist"][1]-ymilieu)**2
    if ppos["r_wrist"][1]<yright:
        constraint = constraint + (ppos["r_wrist"][1]-yright)**2
    if ppos["r_wrist"][0]<xback:
        constraint = constraint + (ppos["r_wrist"][0]-xback)**2
    if ppos["r_wrist"][0]>xfront:
        constraint = constraint + (ppos["r_wrist"][0]-xfront)**2
        
    return {'constraint':constraint,'upper_arm':-normalized_dot(kuarm,puarm), 'fore_arm':-normalized_dot(kfarm,pfarm), 'hand':-normalized_dot(khand,phand)}
    
def cost_l_leg(q_l_leg,kpos):
    ymilieu = 0.02
    yleft = 0.45
    xback = -0.20
    xfront = 0.40
    ppos = MGD_l_leg(q_l_leg)
    kthigh = kpos["l_knee"]-kpos["l_hip"]
    pthigh = ppos["l_knee"]-ppos["l_hip"]
    kleg = kpos["l_ankle"]-kpos["l_knee"]
    pleg = ppos["l_ankle"]-ppos["l_knee"]
    kfoot = kpos["l_toe"]-kpos["l_ankle"]
    pfoot = ppos["l_toe"]-ppos["l_ankle"]
    
    constraint = 0
    if ppos["l_ankle"][1]<ymilieu:
        constraint = constraint + (ppos["l_ankle"][1]-ymilieu)**2
    if ppos["l_ankle"][1]>yleft:
        constraint = constraint + (ppos["l_ankle"][1]-yleft)**2
    if ppos["l_ankle"][0]<xback:
        constraint = constraint + (ppos["l_ankle"][0]-xback)**2
    if ppos["l_ankle"][0]>xfront:
        constraint = constraint + (ppos["l_ankle"][0]-xfront)**2
        
    if ppos["l_toe"][1]<ymilieu:
        constraint = constraint + (ppos["l_toe"][1]-ymilieu)**2
    if ppos["l_toe"][1]>yleft:
        constraint = constraint + (ppos["l_toe"][1]-yleft)**2
    if ppos["l_toe"][0]<xback:
        constraint = constraint + (ppos["l_toe"][0]-xback)**2
    if ppos["l_toe"][0]>xfront:
        constraint = constraint + (ppos["l_toe"][0]-xfront)**2

    return {'constraint':constraint, 'thigh':-normalized_dot(kthigh,pthigh),'leg':-normalized_dot(kleg,pleg)}

def cost_r_leg(q_r_leg,kpos):
    ymilieu = -0.02
    yright = -0.45
    xback = -0.20
    xfront = 0.40
    ppos = MGD_r_leg(q_r_leg)
    kthigh = kpos["r_knee"]-kpos["r_hip"]
    pthigh = ppos["r_knee"]-ppos["r_hip"]
    kleg = kpos["r_ankle"]-kpos["r_knee"]
    pleg = ppos["r_ankle"]-ppos["r_knee"]
    kfoot = kpos["r_toe"]-kpos["r_ankle"]
    pfoot = ppos["r_toe"]-ppos["r_ankle"]
    
    constraint = 0
    if ppos["r_ankle"][1]>ymilieu:
        constraint = constraint + (ppos["r_ankle"][1]-ymilieu)**2
    if ppos["r_ankle"][1]<yright:
        constraint = constraint + (ppos["r_ankle"][1]-yright)**2
    if ppos["r_ankle"][0]<xback:
        constraint = constraint + (ppos["r_ankle"][0]-xback)**2
    if ppos["r_ankle"][0]>xfront:
        constraint = constraint + (ppos["r_ankle"][0]-xfront)**2
        
    if ppos["r_toe"][1]>ymilieu:
        constraint = constraint + (ppos["r_toe"][1]-ymilieu)**2
    if ppos["r_toe"][1]<yright:
        constraint = constraint + (ppos["r_toe"][1]-yright)**2
    if ppos["r_toe"][0]<xback:
        constraint = constraint + (ppos["r_toe"][0]-xback)**2
    if ppos["r_toe"][0]>xfront:
        constraint = constraint + (ppos["r_toe"][0]-xfront)**2

    return {'constraint':constraint, 'thigh':-normalized_dot(kthigh,pthigh),'leg':-normalized_dot(kleg,pleg)}
    
    
def cost_derivative(q,member,kpos):
    deltaq = 1.0
    Kconstraint = 40.0
    if member == "left arm":
        f = cost_l_arm
    elif member == "right arm":
        f = cost_r_arm
    elif member == "left leg":
        f = cost_l_leg
    elif member == "right leg":
        f = cost_r_leg
    dc = 0.0*q
    for i in range(len(dc)):
        dq = 0.0*q
        dq[i] = deltaq
        cplus = f(q+dq,kpos)
        cmoins = f(q-dq,kpos)
        czero = f(q,kpos)
        fplus=fmoins=fzero = 0.
        if "arm" in member:
            if i == 0 or i == 1:
                fplus=fplus+cplus['upper_arm']
                fmoins=fmoins+cmoins['upper_arm']
                fzero=fzero+czero['upper_arm']
            elif i == 2 or i == 3:
                fplus=fplus+cplus['fore_arm']
                fmoins=fmoins+cmoins['fore_arm']
                fzero=fzero+czero['fore_arm']
            elif i == 4 or i == 5:
                fplus=fplus+cplus['hand']
                fmoins=fmoins+cmoins['hand']
                fzero=fzero+czero['hand']
        elif "leg" in member:
            if i == 0 or i == 1 or i == 2:
                fplus=fplus+cplus['thigh']
                fmoins=fmoins+cmoins['thigh']
                fzero=fzero+czero['thigh']
            elif i == 3:
                fplus=fplus+cplus['leg']
                fmoins=fmoins+cmoins['leg']
                fzero=fzero+czero['leg']
        fplus=fplus+Kconstraint*cplus['constraint']
        fmoins=fmoins+Kconstraint*cmoins['constraint']
        fzero=fzero+Kconstraint*czero['constraint']        
        if fzero<fplus and fzero<fmoins:
            dc[i] = 0.0
        else:
            dc[i] = (fplus-fmoins)/(2*deltaq)
    return dc